# gitlab-issue-csv-from-json

## usage

```sh
echo '{"issues":[{"title":"ok"}]}' | npx -q gitlab:ytoune/gitlab-issue-csv-from-json >| issues.csv
```

## interface

```ts
interface Issue {
	title: string
	description?: string
	due?: string
	label?: string[]
	assign?: string
	milestone?: string
	weight?: string
	relate?: string[]
}

interface Issues = {
	issues: Issue[]
}
```

## example

```json
{
  "issues": [
    {
      "title": "make headers readable",
      "description": "- [ ] change text size",
      "due": "in 1 month",
      "label": ["Doing"],
      "assign": "me",
      "milestone": "correction-of-appearance-20200831",
      "weight": 60,
      "relate": "#290",
    }, {
      "title": "make the logo cool",
      "description": "- [ ] change line color\n- [ ] change line weight",
      "due": "in 1 month",
      "label": ["To Do"],
      "assign": "@ytoune",
      "milestone": "correction-of-appearance-20200831",
      "weight": 60,
      "relate": "#290",
    }
  ]
}
```
