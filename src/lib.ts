import { Issue, IssuesCSV } from 'gitlab-issue-csv'
import { Result } from './Result'
import { isIssuesJSON } from './validators'

const parseJSON = (json: string): Result<unknown, SyntaxError> => {
	try {
		return { v: JSON.parse(json) }
	} catch (err) {
		return { err }
	}
}

export const fromJSON = (json: string) => {
	const j = parseJSON(json)
	if (j.err) return { err: j.err }
	const r = isIssuesJSON(j.v)
	if (r.err) return { err: r.err }
	return { v: new IssuesCSV({ issues: r.v.issues.map(i => new Issue(i)) }) }
}
