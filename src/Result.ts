export type Ok<T> = {
	v: T
	err?: undefined
}

export type Err<E> = {
	v?: undefined
	err: E
}

export type Result<T, E> = Ok<T> | Err<E>
