import { IssueJSON, IssuesJSON } from 'gitlab-issue-csv'
import * as IssueJSONSchema from '../schemas/IssueJSON.json'
import * as IssuesJSONSchema from '../schemas/IssuesJSON.json'

import { create } from './create'

export const { validate: isIssueJSON } = create<IssueJSON>(IssueJSONSchema)
export const { validate: isIssuesJSON } = create<IssuesJSON>(IssuesJSONSchema)
