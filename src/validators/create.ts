import Ajv from 'ajv'
import { Result } from '../Result'

interface ValidateFunction<T> {
	(
		data: unknown,
		dataPath?: string,
		parentData?: object | Array<unknown>,
		parentDataProperty?: string | number,
		rootData?: object | Array<unknown>,
	): data is T
	schema?: object | boolean
	errors?: null | Array<Ajv.ErrorObject>
	refs?: object
	refVal?: Array<unknown>
	root?: ValidateFunction<T> | object
	$async?: undefined
	source?: object
}

export const create = <T>(schema: object | object[]) => {
	const ajv = new Ajv()
	const validate_ = ajv.compile(schema) as ValidateFunction<T>
	if (undefined !== validate_.$async)
		throw new Error('asynchronous validation is not supported.')
	const validate = (data: unknown): Result<T, Ajv.ErrorObject[]> =>
		validate_(data)
			? { v: data }
			: validate_.errors
			? { err: validate_.errors }
			: (null as never)
	return { validate }
}
