#!/usr/bin/env node
import { createInterface } from 'readline'
import { fromJSON } from './lib'

const lines: string[] = []

class ValidationError extends Error {}

const main = async () => {
	const input = lines.join('\n')
	const r = fromJSON(input)
	if (!r.err) return console.log(r.v.build())
	if (r.err instanceof SyntaxError) throw r.err
	throw new ValidationError(r.err[0].message)
}

createInterface({ input: process.stdin })
	.on('line', line => lines.push(line))
	.on('close', () => {
		main().catch(x => {
			// console.log('# something happens.')
			console.error(x)
			if ('undefined' === typeof process) return
			process.exit(1)
		})
	})
