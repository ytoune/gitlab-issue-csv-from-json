import * as tjs from 'typescript-json-schema'
import * as path from 'path'
import * as fs from 'fs-extra'

const __rootdir = path.join(__dirname, '..')

const tsconfigPath = path.join(__rootdir, 'tsconfig.json')
const program = tjs.programFromConfig(tsconfigPath)
const settings: tjs.PartialArgs = {
	required: true,
	noExtraProps: true,
}

const generator = tjs.buildGenerator(program, settings)
if (!generator) throw new Error('failed to build generator')

const names = ['IssueJSON', 'IssuesJSON']

const main = async (names: readonly string[]) => {
	const schemadir = path.join(__rootdir, 'src/schemas')
	await fs.remove(schemadir).catch(Boolean)
	await fs.mkdir(schemadir)
	await Promise.all(
		names.map(async name => {
			const schema = generator.getSchemaForSymbol(name)
			await fs.writeJSON(path.join(schemadir, name + '.json'), schema)
		}),
	)
}

main(names).catch(x => {
	console.log('# something happens.')
	console.error(x)
	process.exit(1)
})
